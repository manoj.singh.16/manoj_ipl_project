/**
 * This moudule accepts matches and deliveries data to return strike rate of the provided player for all seasons
 * @module strikeRate
 */

/**
 *
 * @param {Array.<Object>} matchesData parsed JSON data of matches
 * @param {Array.<Object>} deliveriesData parsed JSON data of deliveries
 * @param {string} player Player name whose strike rate to find
 * @returns {Object} strike rate of the provided player for all seasons
 */

const strikeRate = (matchesData, deliveriesData, player = 'V Kohli') => {
  if (Array.isArray(matchesData) && Array.isArray(deliveriesData)) {
    const seasonWithMatchId = matchesData.reduce((acc, match) => {
      acc[match.id] = match.season;

      return acc;
    }, {});

    return deliveriesData
      .filter((delivery) => delivery.batsman === player)
      .reduce((acc, delivery) => {
        if (acc[delivery.batsman] === undefined) {
          acc[delivery.batsman] = {};
        }

        if (acc[delivery.batsman][seasonWithMatchId[delivery.match_id]] === undefined) {
          acc[delivery.batsman][seasonWithMatchId[delivery.match_id]] = {
            runs: 0,
            balls: 0,
          };
        }

        acc[delivery.batsman][seasonWithMatchId[delivery.match_id]].balls += 1;
        acc[delivery.batsman][seasonWithMatchId[delivery.match_id]].runs += parseInt(delivery.batsman_runs);

        acc[delivery.batsman][seasonWithMatchId[delivery.match_id]].economy = (
          (acc[delivery.batsman][seasonWithMatchId[delivery.match_id]].runs * 100) /
          acc[delivery.batsman][seasonWithMatchId[delivery.match_id]].balls
        ).toFixed(2);

        return acc;
      }, {});
  } else {
    return {};
  }
};

module.exports = strikeRate;
