const fs = require('fs');

/**
 * This module accepts data and save output to JSON files with the provided filename to the provided directory.
 * @module saveOutput
 */

/**
 * @param {JSON} data Data to write in file
 * @param {string} filename Filename in which the data is to be stored.
 * @param {string} filepath Directory where the file is to be stored
 * @returns {undefined}
 */

const saveOutput = (data, filename, filepath) => {
  const jsonData = JSON.stringify(data, null, 2);

  fs.writeFile(`${filepath}/${filename}`, jsonData, (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log(`${filename} saved.`);
    }
  });
};

module.exports = saveOutput;
