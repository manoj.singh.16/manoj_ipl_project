/**
 * This moudule accepts matches and deliveries data to return an object with highest number of times one player has been dismissed by another player
 * @module playerDismissed
 */

/**
 *
 * @param {Array.<Object>} matchesData parsed JSON data of matches.
 * @param {Array.<Object>} deliveriesData parsed JSON data of deliveries.
 * @returns {Object} an object with highest number of times one player has been dismissed by another player
 */

const playerDismissed = (deliveriesData, dismissedPlayer = 'V Kohli') => {
  if (Array.isArray(deliveriesData)) {
    const result = deliveriesData
      .filter((delivery) => delivery['player_dismissed'] === dismissedPlayer)
      .reduce((acc, delivery) => {
        if (acc[delivery.bowler] === undefined) {
          acc[delivery.bowler] = 0;
        }

        acc[delivery.bowler] += 1;

        return acc;
      }, {});

    const dismissedBy = Object.entries(result)
      .sort((a, b) => a[1] - b[1])
      .pop();

    return {
      dismissedPlayer,
      dismissedBy: dismissedBy[0],
      times: dismissedBy[1],
    };
  } else {
    return {};
  }
};

module.exports = playerDismissed;
