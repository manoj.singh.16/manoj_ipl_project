/**
 * This moudule accepts matches and deliveries data to return the object with top 10 economical bowlers in the given year
 * @module economicalBowlers
 */

/**
 * the object with match id as keys for all the matches in given year
 * {@link module:matchesInGivenYear}
 * @type {Object}
 * @returns an object with top 10 economical bowlers in the given year
 */
const calcEconomy = require('./calcEconomy');
const matchesInGivenYear = require('./matchesInGivenYear');

/**
 *
 * @param {Array.<Object>} matchesData parsed JSON data of matches
 * @param {Array.<Object>} deliveriesData parsed JSON data of deliveries
 * @param {number|string} year year in which to get top economical bowlers
 * @returns {Object}
 */

const economicalBowlers = (matchesData, deliveriesData, year = 2015) => {
  if (Array.isArray(matchesData) && Array.isArray(deliveriesData)) {
    // matchesInGivenYear returns the object with match id as keys, so that lookup is O(1).
    const matchesById = matchesInGivenYear(matchesData, year);

    const bowlersData = deliveriesData.filter((delivery) => matchesById[delivery.match_id] !== undefined);

    const economyOfPlayers = calcEconomy(bowlersData);

    return Object.fromEntries(
      Object.entries(economyOfPlayers)
        .sort((a, b) => a[1].economy - b[1].economy)
        .slice(0, 10)
    );
  } else {
    return {};
  }
};

module.exports = economicalBowlers;
