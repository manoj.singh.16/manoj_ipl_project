/**
 * This moudule accepts matches data to return Number of matches played per year for all the years
 * @module matchesPerYear
 */

/**
 *
 * @param {Array.<Object>} matchesData parsed JSON data of matches.
 * @returns {Object} number of matches played per year for all the years
 */
const matchesPerYear = (matchesData) => {
  if (Array.isArray(matchesData)) {
    return matchesData.reduce((acc, match) => {
      if (acc[match.season] === undefined) {
        acc[match.season] = 0;
      }

      acc[match.season] += 1;

      return acc;
    }, {});
  } else {
    return {};
  }
};

module.exports = matchesPerYear;
