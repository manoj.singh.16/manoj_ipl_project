/**
 * This module contains contains the default variables used in the project.
 * @module config
 */

const path = require('path');

/**
 * This contains the path to the matches.csv file
 *
 * @type {string}
 */
const matchesCSVPath = path.join(__dirname, '../', 'data/matches.csv');

/**
 * This contains the path to the deliveries.csv file
 *
 * @type {string}
 */
const deliveriesCSVPath = path.join(__dirname, '../', 'data/deliveries.csv');

/**
 * This contains the path to the output directory
 *
 * @type {string}
 */
const outputPath = path.join(__dirname, '../', 'public/output');

/**
 * This contains the year in which to find the economy rate of the bowler
 *
 * @type {number}
 */
const economicalBowlerYear = 2015;

/**
 * This contains the year in which to find extra runs conceded per team
 *
 * @type {number}
 */
const extraRunsYear = 2016;

/**
 * This contains the name of the player (batsman), whose strike rate to calculate.
 *
 * @type {string}
 */
const batsman = 'S Dhawan';

/**
 * This contains the name of the player who is dismissed by another player.
 *
 * @type {string}
 */
const dismissedPlayer = 'MS Dhoni';

module.exports = {
  matchesCSVPath,
  deliveriesCSVPath,
  extraRunsYear,
  economicalBowlerYear,
  outputPath,
  batsman,
  dismissedPlayer,
};
