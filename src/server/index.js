/**
 * @file src/server/index.js is the root file for the project
 * @author Manoj Singh
 * @see <a href="https://gitlab.com/manoj.singh.16/manoj_ipl_project">Gitlab Repository</a>
 */

const csv = require('csvtojson');
const bestSuperoverEconomy = require('./bestSuperoverEconomy');

const { matchesCSVPath, deliveriesCSVPath, extraRunsYear, economicalBowlerYear, outputPath, batsman, dismissedPlayer } = require('./config');
const economicalBowlers = require('./economicalBowlers');
const extraRunsPerTeam = require('./extraRunsPerTeam');
const matchesPerYear = require('./matchesPerYear');
const matchesWonPerTeam = require('./matchesWonPerTeam');
const matchesWonWithToss = require('./matchesWonWithToss');
const playerDismissed = require('./playerDismissed');
const playerOfTheMatch = require('./playerOfTheMatch');
const saveOutput = require('./saveOutput');
const strikeRate = require('./strikeRate');

csv()
  .fromFile(matchesCSVPath)
  .then((matchesData) => {
    const matchesPerYearResult = matchesPerYear(matchesData);
    saveOutput(matchesPerYearResult, 'matchesPerYearResult.json', outputPath);

    const matchesWonPerTeamResult = matchesWonPerTeam(matchesData);
    saveOutput(matchesWonPerTeamResult, 'matchesWonPerTeamResult.json', outputPath);

    const matchesWonWithTossResult = matchesWonWithToss(matchesData, dismissedPlayer);
    saveOutput(matchesWonWithTossResult, 'matchesWonWithTossResult.json', outputPath);

    const playerOfTheMatchResult = playerOfTheMatch(matchesData);
    saveOutput(playerOfTheMatchResult, 'playerOfTheMatchResult.json', outputPath);

    csv()
      .fromFile(deliveriesCSVPath)
      .then((deliveriesData) => {
        const extraRunsPerTeamResult = extraRunsPerTeam(matchesData, deliveriesData, extraRunsYear);
        saveOutput(extraRunsPerTeamResult, 'extraRunsPerTeamResult.json', outputPath);

        const economicalBowlersResult = economicalBowlers(matchesData, deliveriesData, economicalBowlerYear);
        saveOutput(economicalBowlersResult, 'economicalBowlersResult.json', outputPath);

        const strikeRateResult = strikeRate(matchesData, deliveriesData, batsman);
        saveOutput(strikeRateResult, 'strikeRateResult.json', outputPath);

        const playerDismissedResult = playerDismissed(deliveriesData, dismissedPlayer);
        saveOutput(playerDismissedResult, 'playerDismissedResult.json', outputPath);

        const bestSuperoverEconomyResult = bestSuperoverEconomy(deliveriesData);
        saveOutput(bestSuperoverEconomyResult, 'bestSuperoverEconomyResult.json', outputPath);
      })
      .catch((error) => console.log(error));
  })
  .catch((error) => console.log(error));
