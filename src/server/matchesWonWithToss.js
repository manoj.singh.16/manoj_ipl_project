/**
 * This moudule accepts matches data to return number of times each team won the toss and also won the match
 * @module matchesWonWithToss
 */

/**
 *
 * @param {Array.<Object>} matchesData parsed JSON data of matches.
 * @returns {Object} number of times each team won the toss and also won the match
 */

const matchesWonWithToss = (matchesData) => {
  if (Array.isArray(matchesData)) {
    return matchesData
      .filter((match) => match.winner === match.toss_winner)
      .reduce((acc, match) => {
        if (acc[match.winner] === undefined) {
          acc[match.winner] = 0;
        }

        acc[match.winner] += 1;

        return acc;
      }, {});
  } else {
    return {};
  }
};

module.exports = matchesWonWithToss;
