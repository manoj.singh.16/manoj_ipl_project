/**
 * This module accept matches data and return an object with players who has won the highest number of Player of the Match awards for each season.
 * @module playerOfTheMatch
 */

/**
 *
 * @param {Array.<Object>} matchesData parsed JSON data of matches.
 * @returns {Object} an object with players who has won the highest number of Player of the Match awards for each season.
 */

const playerOfTheMatch = (matchesData) => {
  if (Array.isArray(matchesData)) {
    const playerOfTheMatch = matchesData.reduce((acc, match) => {
      if (acc[match.season] === undefined) {
        acc[match.season] = {};
      }

      if (acc[match.season][match.player_of_match] === undefined) {
        acc[match.season][match.player_of_match] = 0;
      }

      acc[match.season][match.player_of_match] += 1;

      return acc;
    }, {});

    Object.keys(playerOfTheMatch).forEach((season) => {
      const result = Object.entries(playerOfTheMatch[season])
        .sort((a, b) => a[1] - b[1])
        .pop();
      playerOfTheMatch[season] = {
        [result[0]]: result[1],
      };
    });

    return playerOfTheMatch;
  } else {
    return {};
  }
};

module.exports = playerOfTheMatch;
