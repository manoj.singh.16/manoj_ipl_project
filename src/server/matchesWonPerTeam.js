/**
 * This moudule accepts matches data to return number of matches won per team per year
 * @module matchesWonPerTeam
 */

/**
 *
 * @param {Array.<Object>} matchesData parsed JSON data of matches.
 * @returns {Object} number of matches won per team per year
 */

const matchesWonPerTeam = (matchesData) => {
  if (Array.isArray(matchesData)) {
    return matchesData.reduce((acc, match) => {
      if (acc[match.season] === undefined) {
        acc[match.season] = {};
      }

      if (acc[match.season][match.winner] === undefined) {
        acc[match.season][match.winner] = 1;
      } else {
        acc[match.season][match.winner] += 1;
      }

      return acc;
    }, {});
  } else {
    return {};
  }
};

module.exports = matchesWonPerTeam;
