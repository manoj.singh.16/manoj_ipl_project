/**
 * This moudule accepts matches data to return the object with match id as keys for all the matches in given year.
 * @module matchesInGivenYear
 */

/**
 *
 * @param {Array.<Object>} matchesData parsed JSON data of matches
 * @param {number|string} year year in which to get matches
 * @returns {Object} an object with match id as keys for all the matches in given year.
 */

const matchesInGivenYear = (matchesData, year) => {
  if (Array.isArray(matchesData)) {
    return matchesData
      .filter((match) => match.season === year.toString())
      .reduce((acc, match) => {
        acc[match.id] = match;

        return acc;
      }, {});
  } else {
    return {};
  }
};

module.exports = matchesInGivenYear;
