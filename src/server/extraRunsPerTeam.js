/**
 * This moudule accepts matches and deliveries data to return the object with extra runs conceded per team in the given year.
 * @module extraRunsPerTeam
 */

/**
 * the object with match id as keys for all the matches in given year
 * {@link module:matchesInGivenYear}
 * @type {Object}
 * @returns {Object} an object with extra runs conceded per team in the given year.
 */
const matchesInGivenYear = require('./matchesInGivenYear');

/**
 *
 * @param {Array.<Object>} matchesData parsed JSON data of matches
 * @param {Array.<Object>} deliveriesData parsed JSON data of deliveries
 * @param {number|string} year year in which to get extra runs
 * @returns {Object}
 */
const extraRunsPerTeam = (matchesData, deliveriesData, year = 2016) => {
  if (Array.isArray(matchesData) && Array.isArray(deliveriesData)) {
    const matchesById = matchesInGivenYear(matchesData, year);

    return deliveriesData
      .filter((delivery) => matchesById[delivery.match_id] !== undefined)
      .reduce((acc, delivery) => {
        // here the lookup is O(1). if we used another loop here then there will be lot of redundant iterations, as the lookup in arrays is O(n)
        if (acc[delivery.bowling_team] === undefined) {
          acc[delivery.bowling_team] = 0;
        }

        acc[delivery.bowling_team] += parseInt(delivery.extra_runs);

        return acc;
      }, {});
  } else {
    return {};
  }

  // matchesInGivenYear returns the object with match id as keys, so that lookup is O(1).
};

module.exports = extraRunsPerTeam;
