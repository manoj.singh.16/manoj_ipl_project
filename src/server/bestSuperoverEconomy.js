/**
 * This moudule accepts deliveries data to return the object with top 10 economical bowlers in the given year
 * @module bestSuperoverEconomy
 */

const calcEconomy = require('./calcEconomy');

/**
 *
 * @param {Array.<Object>} deliveriesData parsed JSON data of deliveries.
 * @returns {Object} the object with top 10 economical bowlers in the given year
 */

const bestSuperoverEconomy = (deliveriesData) => {
  if (Array.isArray(deliveriesData)) {
    const bowlersData = deliveriesData.filter((delivery) => delivery.is_super_over === '1');

    const economyOfPlayers = calcEconomy(bowlersData);

    const bestEconomy = Object.entries(economyOfPlayers)
      .sort((a, b) => b[1].economy - a[1].economy)
      .pop();

    return {
      player: bestEconomy[0],
      economyRate: bestEconomy[1],
    };
  } else {
    return {};
  }
};

module.exports = bestSuperoverEconomy;
