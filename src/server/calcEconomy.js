/**
 * This moudule accepts deliveries data to return calculated economy of bowlers.
 * @module calcEconomy
 */

/**
 *
 * @param {Array.<Object>} bowlersData parsed JSON data of deliveries
 * @returns {Object} an object with calculated economy of bowlers.
 */

const calcEconomy = (bowlersData) => {
  if (Array.isArray(bowlersData)) {
    return bowlersData.reduce((acc, delivery) => {
      // here the lookup is O(1). if we used another loop here then there will be lot of redundant iterations, as the lookup in arrays is O(n)
      if (acc[delivery.bowler] === undefined) {
        acc[delivery.bowler] = {
          balls: 0,
          runs: 0,
        };
      }

      acc[delivery.bowler].balls += 1;
      acc[delivery.bowler].runs += parseInt(delivery.total_runs) - parseInt(delivery.bye_runs) - parseInt(delivery.legbye_runs);

      acc[delivery.bowler].economy = parseFloat((acc[delivery.bowler].runs / (acc[delivery.bowler].balls / 6)).toFixed(2));

      return acc;
    }, {});
  } else {
    return {};
  }
};

module.exports = calcEconomy;
