## Getting Started

First, clone the repository:

```bash
git clone https://gitlab.com/manoj.singh.16/manoj_ipl_project.git
```

then, cd into the project directory

```bash
cd manoj_ipl_project/
```

then, install all the dependencies

```bash
npm i
```

finally, run the project

```bash
npm start
```

## Documentation

Documentation is hosted at [https://manoj.singh.16.gitlab.io/manoj_ipl_project/][1]

## Config file

The config file contains the default variables used in the project.

`matchesCSVPath`: absolute path to the matches.csv file.\
`deliveriesCSVPath`: absolute path to the deliveries.csv file.\
`outputPath`: result output directory.\
`economicalBowlerYear`: the year in which to get economical bowlers.\
`extraRunsYear`: the year in which to get extra runs conceded per team.\

Config file path:

```
/src/server/config.js
```

## Output

The output of the project is saved in a folder in `JSON` format.

Output folder path:

```
/src/public/output
```

[1]: https://manoj.singh.16.gitlab.io/manoj_ipl_project/
